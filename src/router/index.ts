import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/about',
      name: 'about',

      component: () => import('../views/AboutView.vue')
    },
    {
      path: '/menu2',
      name: 'menu2',
      component: () => import('../views/Menu2View.vue')
    },
    {
      path: '/Mas menus',
      name: 'Mas menus',
      component: () => import('../views/MasView.vue')
    },
    {
      path: '/Menu4view',
      name: 'Menu4View',
      component: () => import('../views/Menu4View.vue')
    },
    {
      path: '/Otro menu',
      name: 'Otro menu',
      component: () => import('../views/OtroMenuView.vue')
    },
    {
      path: '/Menu3View',
      name: 'Menu3View',
      component: () => import('../views/Menu3View.vue')
    }
  ]
})

export default router
